const jwt = require('jsonwebtoken')
const vars = require('../constants')

module.exports = (req, res, next) => {
  const header = req.headers.authorization

  if (header) {
    const token = header.split(' ')[1]
    const decodedToken = jwt.verify(token, vars.SECRET)
    req.userId = decodedToken.id
    next()
  } else {
    res.status(401).json({ error: 'Token inválido' })
  }
}
