const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const vars = require('./constants')
const userRoutes = require('./user/routes')
const bbqRoutes = require('./bbq/routes')

const app = express()

app.use(cors())
app.use(bodyParser.json())

app.listen(vars.PORT, () => {
  userRoutes(app)
  bbqRoutes(app)
  console.log(`Rodando na porta ${vars.PORT}`)
})
