const controller = require('./controller')
const checkAuth = require('../helpers/auth')

module.exports = (app) => {
  app.get('/bbq/:bbqId', checkAuth, (req, res) => {
    const { bbqId } = req.params
    controller.getBbqDetails(bbqId).then(bbq => {
      if (bbq) {
        res.status(200).json({ bbq })
      } else {
        res.status(404).json({ error: 'Churras não encontrado' })
      }
    })
  })

  app.get('/bbqs', checkAuth, (req, res) => {
    controller.getBbqs().then(bbqs => {
      if (bbqs) {
        res.status(200).json({ bbqs })
      } else {
        res.status(404).json({ error: 'Nenhum churras marcado' })
      }
    })
  })
}
