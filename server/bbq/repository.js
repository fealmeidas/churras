const db = require('../db/models')
const { Bbq } = db

const findById = id => {
  return Bbq.findByPk(id)
}

const getBbqs = () => {
  return Bbq.findAll()
}

module.exports = { findById, getBbqs }
