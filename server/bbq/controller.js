const service = require('./service')

const getBbqDetails = bbqId => {
  return service.getBbqDetails(bbqId)
}

const getBbqs = () => {
  return service.getBbqs()
}

module.exports = { getBbqDetails, getBbqs }
