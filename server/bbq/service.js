const repository = require('./repository')

const getBbqDetails = bbqId => {
  return repository.findById(bbqId).then(bbq => {
    if (bbq) {
      return bbq.getUsers().then(users => {
        return {
          description: bbq.description,
          date: bbq.date,
          suggestedContributionWithDrinks: bbq.suggestedContributionWithDrinks,
          suggestedContributionWithoutDrinks: bbq.suggestedContributionWithoutDrinks,
          users: users.map(user => ({
            id: user.id,
            name: user.name,
            contribution: user.UserBbq.contribution,
            confirmed: user.UserBbq.confirmed
          }))
        }
      })
    }
  })
}

const getBbqs = () => {
  return repository.getBbqs()
}

module.exports = { getBbqDetails, getBbqs }
