'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserBbq = sequelize.define('UserBbq', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    contribution: DataTypes.DECIMAL,
    confirmed: DataTypes.BOOLEAN
  }, {});
  UserBbq.associate = function(models) {
    // associations can be defined here
  };
  return UserBbq;
};
