'use strict';

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const vars = require('../../constants');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
    User.belongsToMany(models.Bbq, { through:'UserBbq' })
  };

  User.prototype.matchPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
  };

  User.prototype.generateToken = function () {
    return jwt.sign({ id: this.id }, vars.SECRET);
  }

  return User;
};
