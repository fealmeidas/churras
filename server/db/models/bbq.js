'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bbq = sequelize.define('Bbq', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    description: DataTypes.STRING,
    date: DataTypes.DATE,
    suggestedContributionWithDrinks: DataTypes.DECIMAL,
    suggestedContributionWithoutDrinks: DataTypes.DECIMAL
  }, {});
  Bbq.associate = function(models) {
    // associations can be defined here
    Bbq.belongsToMany(models.User, { through:'UserBbq' })
  };
  return Bbq;
};
