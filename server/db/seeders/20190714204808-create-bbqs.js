'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Bbqs', [{
        description: 'Aniver do Gui',
        date: new Date(),
        suggestedContributionWithDrinks: 20.0,
        suggestedContributionWithoutDrinks: 10.0,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        description: 'Aniver',
        date: new Date(),
        suggestedContributionWithDrinks: 20.0,
        suggestedContributionWithoutDrinks: 10.0,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
