'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('UserBbqs', [{
      userId: 1,
      bbqId: 1,
      contribution: 20.0,
      confirmed: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      userId: 2,
      bbqId: 1,
      contribution: 10.0,
      confirmed: false,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      userId: 1,
      bbqId: 2,
      contribution: 20.0,
      confirmed: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
