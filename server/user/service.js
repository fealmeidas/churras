const repository = require('./repository')

const login = (email, password) => {
  return repository.findByEmail(email).then(user => {
    if (user) {
      if (user.matchPassword(password)) {
        return user.generateToken()
      }
      return null
    }
  })
}

module.exports = { login }
