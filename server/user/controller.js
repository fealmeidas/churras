const service = require('./service')

const login = (email, password) => {
  return service.login(email, password)
}

module.exports = { login }
