const controller = require('./controller')

module.exports = (app) => {
  app.post('/login', (req, res) => {
    const { email, password } = req.body
    controller.login(email, password).then(token => {
      if (token) {
        res.status(200).json({ token })
      } else {
        res.status(401).json({ error: 'Email ou senha inválidos' })
      }
    })
  })
}
