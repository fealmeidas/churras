const db = require('../db/models')
const { User } = db

const findByEmail = email => {
  return User.findOne({ where: { email } })
}

module.exports = { findByEmail }
