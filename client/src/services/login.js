import axios from 'axios'

export default credentials => axios.post('http://localhost:3001/login', credentials)
