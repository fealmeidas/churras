import React from 'react'
import { connect } from 'react-redux'

import Input from '../../components/Input/Input'
import Button from '../../components/Button/Button'
import { login } from '../../actions/login'

import './Login.scss'

class Login extends React.Component {
  constructor() {
    super()
    this.state = { email: '', password: '' }
  }

  handleLogin(e) {
    e.preventDefault()
    this.props.login(this.state)
      .then(token => localStorage.setItem('CHURRAS_TOKEN', token))
      .then(this.setState({ email: '', password: '' }))
  }

  render() {
    return (
      <div className='login-screen'>
        <h1 className='header'>Agenda de Churras</h1>
        <form className='login-form'>
          <Input
            name='Login'
            placeholder='e-mail'
            type='email'
            onChange={ e => { this.setState({ email: e.target.value }) } }
            value={this.state.email}
            />
          <Input
            name='Senha'
            placeholder='senha'
            type='password'
            onChange={ e => { this.setState({ password: e.target.value }) } }
            value={this.state.password}
            />
          <Button
            className='login-button'
            text='Entrar'
            onClick={ e => this.handleLogin(e) }
          />
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  login: credentials => dispatch(login(credentials))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
