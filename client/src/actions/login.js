import loginService from '../services/login'

export const LOGIN_START = 'LOGIN_START'
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'

const loginStart = () => ({
  type: LOGIN_START
})

const loginError = () => ({
  type: LOGIN_ERROR
})

const loginSuccess = () => ({
  type: LOGIN_SUCCESS
})

export const login = credentials => dispatch => {
  dispatch(loginStart())
  return loginService(credentials)
  .then(response => {
    dispatch(loginSuccess())
    return response.data.token
  })
  .catch(error => dispatch(loginError()))
}
