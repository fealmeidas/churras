import React from 'react'

import './Input.scss'

export default ({ name, placeholder, onChange, value, type }) => {
  return (
    <div className='input'>
      <label className='input__label'>{ name }</label>
      <input className='input__field' type={type} placeholder={placeholder} onChange={onChange} value={value} />
    </div>
  )
}
