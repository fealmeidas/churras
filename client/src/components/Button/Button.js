import React from 'react'

import './Button.scss'

export default ({ text, onClick }) => {
  return <button className='button' onClick={onClick}>{ text }</button>
}
