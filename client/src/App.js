import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Login from './screens/Login/Login'

function App() {
  return (
    <Switch>
      <Route exact path='/login' component={Login}/>
    </Switch>
  );
}

export default App
